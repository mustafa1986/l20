import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class ListArray {


    public static int userInput(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        int returnNum = 0;
        while (true) {
            String arrayNum = scanner.nextLine();
            try {
                returnNum = Integer.parseInt(arrayNum);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Reqem daxil edin: ");
            }
        }
        return returnNum;
    }


    public static ArrayList<Integer> addlist(int elementNumber) {
        ArrayList<Integer> array = new ArrayList<>();
        Random random = new Random();
        int number;
        for (int i = 0; i < elementNumber; i++) {
            number = random.nextInt(100);
            array.add(number);
        }
        return array;

    }

    public static void writeFile(ArrayList<Integer> arrayList, String message, String path) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("G:\\" + path, true));
            writer.write(message);
            for (int i = 0; i < arrayList.size(); i++) {
                writer.write(" " + arrayList.get(i));
            }
            writer.write("\n");

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Fayl tapilmadi");
        }
    }

    public static ArrayList<Integer> sumlist(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        int arrayMinSize = 0;
        ArrayList<Integer> sumArray = new ArrayList<>();
        if (list1.size() <= list2.size()) {
            arrayMinSize = list1.size();
        } else {
            arrayMinSize = list2.size();
        }
        for (int i = 0; i < arrayMinSize; i++) {
            sumArray.add(list1.get(i) + list2.get(i));
        }
        return sumArray;
    }

    public static ArrayList<Integer> ferqlist(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        int arrayMinSize = 0;
        ArrayList<Integer> ferqarray = new ArrayList<>();
        if (list1.size() <= list2.size()) {
            arrayMinSize = list1.size();
        } else {
            arrayMinSize = list2.size();
        }
        for (int i = 0; i < arrayMinSize; i++) {
            ferqarray.add(list1.get(i) - list2.get(i));
        }
        return ferqarray;
    }
}
