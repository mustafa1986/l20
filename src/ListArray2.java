import java.util.ArrayList;

public class ListArray2 extends ListArray {
    public static ArrayList<Integer> compareArrays(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> compare = new ArrayList<>();
        int minimumArray = (list1.size() < list2.size()) ? list1.size() : list2.size();
        for (int i = 0; i < minimumArray; i++) {
            for (int j = 0; j < minimumArray; j++) {
                if (list1.get(i) == list2.get(j)) {
                    compare.add(list1.get(i));
                }
            }
        }
        if (compare.isEmpty()){
            return null;
        }else
        return compare;
    }

    public static ArrayList<Integer> minMaxElement(ArrayList<Integer> compareList) {
        int min=compareList.get(0);
        int max=compareList.get(0);
        ArrayList<Integer> minMaxArray=new ArrayList<>();
        if(compareList.isEmpty()){
            System.out.println("Daxil edilen Listlerde ortaq element yoxdur.");
        }else{
            for (int i = 0; i < compareList.size(); i++) {
                if (compareList.get(i)<min){
                    min=compareList.get(i);
                }else if (compareList.get(i)>max) {
                    max=compareList.get(i);
                }
            }
        }
        minMaxArray.add(min);
        minMaxArray.add(max);
        return minMaxArray;
     }

}
