import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int firstMassivNumber =ListArray.userInput("1ci reqemi daxil edin: ");
        ArrayList<Integer> firstMassivArray = ListArray.addlist(firstMassivNumber);
        ListArray.writeFile(firstMassivArray, "1ci massiv", "Array1.txt");
        int secondMassivNumber = ListArray.userInput("2ci reqemi daxil edin: ");
        ArrayList<Integer> secondMassivArray = ListArray.addlist(secondMassivNumber);
        ListArray.writeFile(secondMassivArray, "2ci massiv", "Array1.txt");

        //Funksiyaya 1 ve 2 ci massivi gonderirik return cavabi fayla yaziriq
        //ArrayList<Integer> thirdarray=ListArray.sumlist(firstMassivArray,secondMassivArray);
        //ListArray.writeFile(thirdarray,"Toplama array","sumarray.txt");

        //Funksiyaya 1 ve 2 ci massivi gonderirik return cavabi fayla yaziriq
        //ArrayList<Integer> forthArray=ListArray.ferqlist(firstMassivArray,secondMassivArray);
        //ListArray.writeFile(forthArray,"Cixma array","ferqarray.txt");
        ArrayList<Integer> compare=ListArray2.compareArrays(firstMassivArray,secondMassivArray);
        if (compare==null){
            System.out.println("Daxil olunan listde ortaq ededler yoxdur");
        }else{
            ArrayList<Integer> minMax=ListArray2.minMaxElement(compare);
            System.out.println("Arrayin minimal elementi: "+minMax.get(0)+"\nArrayin maksimal elementi: "+minMax.get(1));
        }



    }
}